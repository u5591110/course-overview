#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example from Lecture 3 of reformatting names of RSCS academics
# retrieved from the web in the form of "LASTNAME, Firstname".
#

with open("rscs_academics.txt") as fh:
    for name in fh:
        p = name.find(",")
        if (p == -1): continue
        lastname = name[0:p].strip().title()
        firstname = name[p+1:].strip()
        print("{}. {}".format(firstname[0], lastname))
