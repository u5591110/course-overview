#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Demonstrate the reading of JSON files.

import json

# initialise list of final grades
final_grades = []

# parse JSON file
with open('student_grades.json') as fh:
    students = json.load(fh)
    for student in students:
        student_name = student["name"]
        student_grade = 0.25 * (float(student["asgn1"]) + float(student["asgn2"]) +
            float(student["asgn3"]) + float(student["exam"]))
        final_grades.append([student_name, student_grade])

# print grades
for name, grade in final_grades:
    print("{}\t{}".format(name, grade))
